import string
from random import randint, choice


def main():
    length = 0
    try:
        length = abs(int(input("\nEnter the desired length (up to 512 characters):\n")))
    except:
        print("The value entered is invalid.")
    if length < 1 or length > 512 or length is None:
        main()
    elif length:
        chars = string.ascii_letters + string.punctuation + string.digits
        password = "".join(choice(chars) for x in range(randint(length, length)))
        print(f"\nYour password is as follows:\n{password}")
        again = str(input("\nWould you like to generate another password? [y/n]\n"))
        if again == "y" or again == "Y" or again == "Yes" or again == "YES":
            main()
        else:
            print("Exiting...")


main()
